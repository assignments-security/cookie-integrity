window.addEventListener("load", refresh);
let sensorTable = [];

async function refresh() {
    const settings = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        credentials: 'same-origin'
    };
    const data = await fetch(`https://localhost:8000/refresh`, settings)
        .then(response => response.json())
        .then(json => {
            //            alert(json)
            return json;
        })
        .catch(e => {
            console.log(e)
        });
    if (data)
        updateState(data);
}


setTempOrLight = async (id) => {
    const settings = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        credentials: 'same-origin'
    };
    matchedRoute = sensorTable.filter(route => route.path == id)
    const data = await fetch(`https://localhost:8000/{matchedRoute[0]}`, settings)
        .then(response => response.json())
        .then(json => {
            alert(json)
            return json;
        })
        .catch(e => {
            return e
        });
    return data;
}


clickLight = async (id) => {
    const settings = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        credentials: 'same-origin'
    };
    //    console.log(id)
    matchedRoute = sensorTable.filter(route => route.id == id)[0].path
    console.log(matchedRoute)
    const data = await fetch(`https://localhost:8000${matchedRoute}`, settings)
        .then(response => response.json())
        .then(json => updateState(json))
        // .then(json => {
        //     //alert(json)
        //     return json;
        // })
        .catch(e => {
            console.log(e)
        });
    //    return data;
}

function updateState(state) {
    //    console.log('sensorTable state' + JSON.stringify(state))
    sensorTable = state
    sensorTable.forEach((route) => {
        let el = document.getElementById(route.id)
        if (route.type == 'light')
            el.className = route.state ? 'btn btn-warning btn-sm' : 'btn btn-secondary btn-sm';
        else
            el.textContent = route.state;
    })
}

const getOneSensor = async (sensor) => {
    const settings = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        credentials: 'same-origin'
    };
    matchedRoute = sensorTable.filter(route => route.id == id)[0].path
    const data = await fetch(`https://localhost:8000${matchedRoute}`, settings)
        .then(response => response.json())
        .then(json => console.log(json))
        .catch(e => {
            console.log(e)
        });
}

setInterval(refresh, 5000);
