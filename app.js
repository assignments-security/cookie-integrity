const https = require('https');
const fs = require('fs');
const express = require('express')

const app = express()
//const router = express.Router()
const router = require('./routes');
// const port = process.argv[2] || 8000;

const mustacheExpress = require('mustache-express');
// Register '.mustache' extension with The Mustache Express
const options = {
    key: fs.readFileSync('cert/server.key'),
    cert: fs.readFileSync('cert/server.crt')
};

// app.get('/', (req, res) => res.send('Hello World!'))

// app.listen(port, () => console.log(`Example app listening on port ${port}!`))

// router.get('/', (req, res) => {
//     console.log('app')
//   res.send('Hello HTTPS!')
// })

app.use(express.static('public'))

app.engine('mustache', mustacheExpress());
app.set('view engine', 'mustache');
// app.set('views', __dirname + '/views');
app.set('views', './templates');

app.use(express.urlencoded({ extended: false }))
app.use(express.json()) // edit: båda samtidigt verkar funka

app.use('/', router)

const server = https.createServer(options, app)

//server.listen(parseInt(port));
server.listen(8000, () => console.log('server ready'));
