const encryptCreds = false
const dbService = require('./db-service');
if (encryptCreds) // not required anymore, some work required befor enabling as it uses callbacks
    var easyPbkdf2 = require("easy-pbkdf2")();
const fs = require('fs');
const crypto = require('crypto');
const cookie = require('cookie');
const globalSecret = crypto.randomBytes(64).toString('hex');
const patched = false
// let sessionIds = []; // bir fel i scope o i server?
// let sessionIdCounter = 0 // iom att man tar bort från arrayen

// maybe add DB to functions that read from db?
exports.authenticateLogin = async function(body) {
    console.log('newauth_ ' + JSON.stringify(body))
    console.log('newauth_ ' + body.username)
    let valid = await dbService.login(body.username, body.password)
    console.log('auth login valid: ' + valid)
    let newSessionId = ''
    let hash
    if (valid) {
        newSessionId = await exports.addSessionId();
        hash = createHash(newSessionId, body.username)
    }

    return ({ 'valid': valid, 'sessionId': newSessionId, 'username': body.username, 'hash': hash ? hash : '' });
}

function checkHash(receivedHash, sessionId, username) {
    if (!patched)
        return true
    console.log('checking hash: ' + receivedHash + ' ' + sessionId + ' ' + username)
    return createHash(sessionId, username) === receivedHash
}

function createHash(sessionId, username) {
    let hash = crypto.createHash('sha256');
    hash.update(sessionId + username + globalSecret)
    return hash.digest('hex')
}



exports.addSessionId = async function() {
    let sessionId = crypto.randomBytes(64).toString('hex');
    let res = await dbService.addSessionId(sessionId)
    console.log('auth, res from addsessionid: ' + res)
    return sessionId;
}

exports.authorizeSession = async function(req) { // check if session is in session array
    const cookies = cookie.parse(req.headers.cookie || '');
    //console.log('session in cookie: ' + JSON.stringify(cookies))
    const squeakCookies = cookies['squeak-session'] != null ? JSON.parse(cookies['squeak-session']) : '';
    console.log('session in cookie: ' + JSON.stringify(squeakCookies))
    //    if (req.headers.cookie && sessionIds.includes(squeakCookies.sessionid)) {
    // todo: now object with sessionid and username
    const authorized = await dbService.checkSessionId(squeakCookies.sessionid)
    console.log(JSON.stringify(authorized))
    if (authorized && checkHash(squeakCookies.hash, squeakCookies.sessionid, squeakCookies.username)) {
        return squeakCookies
    }
    else
        return false
}

exports.clearSession = async function(req) {
    console.log('clearing session')
    // clear cookie sent to browswer and take it away from the sessionIds in db
    let cookies = cookie.parse(req.headers.cookie || '');
    console.log(cookies)
    if (cookies['squeak-session']) {
        //let cookie = cookies['squeak-session']
        const squeakCookies = cookies['squeak-session'] != null ? JSON.parse(cookies['squeak-session']) : '';
        console.log(cookie)
        //sessionIds = sessionIds.filter(id => id != squeakCookies.sessionid)
        var res = await dbService.invalidateSession(squeakCookies.sessionid)
    }
    console.log('in clearSession 2, sessionIds ' + JSON.stringify(res))
    //return sessionIds // not the original array!
}

exports.storeNewUser = function(user) {
    // let username = user.username
    // let password = user.password
    console.log('storing new user: ' + user.username + ' ' + user.password)
    dbService.writeNewUser(user.username, user.password)
}

exports.CheckIfNewUserCredentialsValid = function(user) {
    console.log('adduser, hash: ' + user.password_hash)
    console.log('salt: ' + user.salt)
    let namePwdValidObj = { 'nameValid': newUsernameOk(user.username), '.pwdValid': false }
    if (namePwdValidObj.nameValid)
        namePwdValidObj.pwdValid = newPasswOk(user.username, user.password)
    // if (namePwdValidObj.nameValid && namePwdValidObj.pwdValid) {
    //     namePwdValidObj.userAdded = true;
    // } else
    //     namePwdValidObj.userAdded = false;
    return namePwdValidObj;
}

async function newUsernameOk(username) {
    // the name is at least four characters long
    // the name is not already taken
    //    const usernames = JSON.parse(fs.readFileSync('passwd').toString())
    console.log(username)
    //    console.log(JSON.stringify(usernames))
    //   console.log('!usernames.hasOwnProperty(username) ' + !usernames.hasOwnProperty(username))
    let userNameAvailable = await dbService.checkIfUserNameAvailable(username)
    return (username.length > 3 && userNameAvailable && !username.match(/[^a-zA-Z0-9]/)) // ^ negates, so not outside of these
}

// old code from here


// login, read from encrypted file
exports.authenticateLoginFromEncryptedFile = async function(body) {
    console.log('newauth_ ' + JSON.stringify(body))
    const userEntered = body.username
    const userEnteredPassword = body.password //?
    const users = JSON.parse(fs.readFileSync('passwd').toString())

    console.log('userEntered ' + userEntered)
    console.log('userEntered in ' + users[userEntered])
    //    console.log('users ' + JSON.stringify(users))

    if (users[userEntered]) //todo: kolla så inte kraschar
        var user = users[userEntered];
    else
        return Promise.resolve({ 'valid': false, 'sessionId': '', 'username': userEntered });

    // console.log('user ' + user)
    // console.log('user.password_hash ' + user.password_hash)

    return new Promise((resolve, reject) => {
        easyPbkdf2.verify(user.salt, user.password_hash, userEnteredPassword, (err, valid) => {
            console.log('authenticateLogin: ' + err + ' ' + valid);
            if (valid) {
                return resolve({ 'valid': valid, 'sessionId': addSessionIdStoredLocal(), 'username': userEntered });
            } else
                // return err ? reject(err) : resolve(valid);
                return resolve({ 'valid': valid, 'sessionId': '', 'username': userEntered });
        })
    })
}


exports.wrapperAddSessionId = function() { //wrapper for calling from other file
    return addSessionIdStoredLocal()
}

function addSessionIdStoredLocal() { // returns added sessionid
    const sha = crypto.createHash('sha256');
    sha.update(Math.random().toString());
    sessionIds.push(sha.digest('hex'));
    return sessionIds[sessionIds.length - 1]
}




exports.authorizeSessionLocal = function(req) { // check if session is in session array
    const cookies = cookie.parse(req.headers.cookie || '');
    console.log('session in cookie: ' + JSON.stringify(cookies))
    console.log('in auth, sessionIds ' + sessionIds)
    const squeakCookies = cookies['squeak-session'] != null ? JSON.parse(cookies['squeak-session']) : '';
    console.log(req.headers.cookie && sessionIds.includes(squeakCookies.sessionid))
    //    console.log(req.headers.cookie && sessionIds.includes(Number(squeakCookies.sessionid)))
    //return req.headers.cookie && sessionIds.includes(Number(cookies['squeak-session']))
    if (req.headers.cookie && sessionIds.includes(squeakCookies.sessionid)) {
        // todo: now object with sessionid and username
        return squeakCookies
    }
    else
        return false
}

exports.clearSessionLocal = function(req) {
    console.log('clearing session')
    console.log('in clearSession 1, sessionIds ' + sessionIds)
    // clear cookie sent to browswer and take it away from the sessionIds array
    let cookies = cookie.parse(req.headers.cookie || '');
    console.log(cookies)
    if (cookies['squeak-session']) {
        //let cookie = cookies['squeak-session']
        const squeakCookies = cookies['squeak-session'] != null ? JSON.parse(cookies['squeak-session']) : '';
        console.log(cookie)
        sessionIds = sessionIds.filter(id => id != squeakCookies.sessionid)
    }
    console.log('in clearSession 2, sessionIds ' + sessionIds)
    //return sessionIds // not the original array!
}

// below is for regestiering new users
exports.storeNewUserToFile = function(user) {
    let username = user.username
    let password = user.password
    console.log('storing new user: ' + username + ' ' + password)
    const salt = easyPbkdf2.generateSalt();
    // `salt` should be treated as opaque, as it captures iterations
    //const password = "RandomDigits";
    easyPbkdf2.secureHash(password, salt, function(err, passwordHash, originalSalt) {
        // use your own db's methods to save the hashed password AND salt.
        const users = JSON.parse(fs.readFileSync('passwd').toString())
        users[username] = {
            // The Base64 encoded hash, 344 characters long
            "password_hash": passwordHash,
            // Salt length varies based on SALT_SIZE and iterations. The default SALT_SIZE of
            // 32 produces a value that is:
            // (hashIterations.toString(16).length) + 1 + base64EncodedSalt.length)
            // characters long (42 characters).
            "salt": originalSalt // === salt
        }
        fs.writeFileSync('passwd', JSON.stringify(users))
    });
}



exports.CheckIfNewUserCredentialsValidLocal = function(user) {
    console.log('adduser, hash: ' + user.password_hash)
    console.log('salt: ' + user.salt)
    let namePwdValidObj = { 'nameValid': newUsernameOkLocal(user.username), '.pwdValid': false }
    if (namePwdValidObj.nameValid)
        namePwdValidObj.pwdValid = newPasswOk(user.username, user.password)
    // if (namePwdValidObj.nameValid && namePwdValidObj.pwdValid) {
    //     namePwdValidObj.userAdded = true;
    // } else
    //     namePwdValidObj.userAdded = false;
    return namePwdValidObj;
}

function newUsernameOkLocal(username) {
    // the name is at least four characters long
    // the name is not already taken
    const usernames = JSON.parse(fs.readFileSync('passwd').toString())
    console.log(username)
    console.log(JSON.stringify(usernames))
    console.log('!usernames.hasOwnProperty(username) ' + !usernames.hasOwnProperty(username))
    return (username.length > 3 && !usernames.hasOwnProperty(username) && !username.match(/[^a-zA-Z0-9]/)) // ^ negates
}

function newPasswOk(username, passw) {
    // the password is at least eight characters long
    // the password does not contain the name
    let validPassword = passw !== undefined && passw.length >= 8;
    if (validPassword) {
        let nameregex = new RegExp(username);
        validPassword &= !nameregex.test(passw);
    }
    console.log('validPassword ' + validPassword)
    return validPassword
}

