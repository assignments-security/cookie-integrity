## Social media web app ##
A simple social media app I made for a class in security. The idea was to first build the app then explore vulnerabilities.

### NoSQL injection ###
In the web console look at the credentials sent with the form. They are {"username":"{\"$gt\": \"\"}","password":"{\"$gt\": \"\"}"} but when sent through the console they are {"username":{"$gt":""},"password":{"$gt":""}}.

Entering "{"$gt": ""}" through the console  as username and passwords shows all the squeaks (messages sent to all).
To log in as a user to also show his squeals (personal messages), I just have to type his username. Since the code evaluates to true the password is accepted.

To hijack a session the same technique can be used. In the Application tab of Chromes developer tools it is easy to add a cookie called squeak-session and set it to {"username":"H. Curry","sessionid":{"$gt":""}}.

#### Patching ####
Patching for nosql injections in the case of username and session is done by whitelisting the characters that can be used. In the case of the password it should of course be hashed making the injection impossible. But for now it is blacklisting the special characters ' " \ ; { } $

Patching done in db-service.js, a flag 'pathed' can unpatch.

### Cookie integrity attack ###
The cookie sent is authenticated but not tied to a username. Since the username is specified in the cookie, it is a small thing to impersonate another user by changing the username. This allows for reading his private messages and posting in his name.
#### Patching ####
A hash can be used to make sure the cookie has not been tampered with. But in order to prevent impersination the string that is hashed needs to contain a secret. So a key called hash is added to the squeak-session cookie with the hash of the session-id, the username and a secret key.

Patching done in author.js, a flag 'pathed' can unpatch.
